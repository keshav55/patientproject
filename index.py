import pandas as pd
import networkx as networkx
import collections
import pickle
from scipy import stats
from flask import Flask, request, render_template, redirect, abort, flash, jsonify
from flask_util_js import FlaskUtilJs
from flask_jsglue import JSGlue







app = Flask(__name__)

fujs = FlaskUtilJs(app)
jsglue = JSGlue(app)

def auc_score(input):

	return score 

def drug_query(search):
	pkl_file = open('./patientdrug.pkl', 'rb')
	output = pickle.load(pkl_file)
	pkl_file.close()
	list = output.get(search)
	newlist = []
	valuelist = []
	auc =  pd.DataFrame.from_csv('./expression_auc.csv', index_col=0)
	for x in list:
		new = auc[auc['Drug'] == x]
		this = new.AUC.values
		if (this > 0.70):
			newlist.append(new.Drug.values)
			valuelist.append(this)
	result = pd.DataFrame()
	newlist = [l[0] for l in newlist]
	valuelist = [l[0] for l in valuelist]
	result['drug'] = newlist
	result['score'] = valuelist
	
	return result
	# sorted_result = result.sort_values(ascending=False)
	# return sorted_result

    


def query(search):
	patient_matrix = pd.DataFrame.from_csv('./geneadjmatrix.csv')
	single_patient = patient_matrix[search]
	sorted_patient = single_patient.sort_values(ascending=False)
	print(sorted_patient[:5])
	return sorted_patient[:5]

@app.route("/")
def index():
	return render_template('index.html')


@app.route('/similar')
def similar():
	return render_template('similarity.html')



@app.route('/predictor', methods=['GET', 'POST'])

def predictor():

	if request.method == 'POST':
		search = request.form['text']
		# output = drug_query(search)
		output = query(search)
		print(output.values)
		return render_template('predictor.html', patient_names = output.index, mutation_count = output.values, patient = search)
	else:
		return render_template('predictor.html')


# @app.route("/search", methods=['GET', 'POST'])
# def index():

# 	if request.method == 'POST':
# 		search = request.form['text']
# 		result = query(search)
# 		return render_template('search.html', patient_names = result.index, mutation_count = result.values, patient = search) 

# 	else:
# 		return render_template('search.html')


	
	# query(search)
    # return render_template('index.html')

# def query(search):
#     patient_matrix = pd.DataFrame.from_csv('./geneadjmatrix.csv')
#     single_patient = patient_matrix[search]
#     sorted_patient = single_patient.sort_values(ascending=False)
#     return sorted_patient[:5]

@app.context_processor
def utility_processor():
    def query(search):
        patient_matrix = pd.DataFrame.from_csv('./geneadjmatrix.csv')
        single_patient = patient_matrix[search]
        sorted_patient = single_patient.sort_values(ascending=False)
        return sorted_patient[:5]
    
    def predict_drugs(search):
		patient_matrix = pd.DataFrame.from_csv('./genedata/important/spearmanvalues.csv')
		single_patient = patient_matrix[patient_matrix['Cell_Line_1'] == search]
		

    return dict(query=query, predict_drugs=predict_drugs)


if __name__ == "__main__":
    app.run(debug = True)





