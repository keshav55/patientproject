var data = [];
var mutations = [];
var patients = [];
var genes = {};
var people = [];


var db = new PouchDB('cancerdata');





 $(function() {
    $( "input[type=submit], button" )
      .button()
      .click(function( event ) {
        if (!$('#files')[0].files.length)
      {
        alert("Please choose at least one file to parse.");
      }
      else if ($('#files')[0].files.length > 0) {

        event.preventDefault();
        db.get("1").then(function (doc) {
         var $load = '<div class="progress"><div class="indeterminate"></div></div>';
         $($load).insertAfter( ".btn" );

        time();
        })
        .catch(function (err) {
          console.log("error with upload");
          console.log(err);
          populate();

        });
      }
      });
  });


function time() {
  $("input[type=file]").parse({
  config: {
    complete: function(results) {

      var rank = [];

      var newMute = [];

      var newPeople = [];

      for (var i = 0; i < results.data.length; i ++) {
        newMute.push(results.data[i][2]);
      }

      db.allDocs({
        include_docs: true,
        attachments: true
        }).then(function (result) {
          console.log(result);

          for(var g = 0; g <result.rows.length;g++) {
            newPeople.push(result.rows[g].doc.patient);
            rank.push((_.intersection(newMute, result.rows[g].doc.mutations).length));
          }
          console.log(rank);
          score(rank, newPeople);

        }).catch(function (err) {
          console.log(err);
        });


      // for(var g in genes) {
      //   rank.push((_.intersection(newMute, genes[g]).length));
      // }
      //
      // console.log(rank);

      // score(rank, people);
    }
  },
});
}

function taffer(files) {

  var url = Flask.url_for("static", {"filename": "all_maf_8_col.csv"});
  Papa.parse(url, {
        download: true,
        dynamicTyping: true,
        complete: function(data) {


//     var doc = {
//       "_id":"1", "patient": "AAA", "mutations":["arr", "err", "baar"]
//             };
//     db.put(doc);
//
//     db.get("1").then(function (doc) {
//         console.log(doc);
// });


  }
});
}






function populate() {
  var url = Flask.url_for("static", {"filename": "all_maf_8_col.csv"});
  Papa.parse(url, {
        download: true,
        dynamicTyping: true,
        complete: function(data) {

        console.log("just finished download");

        if (db) {
          console.log("we good");
        }

        for(var i = 1; i < data.data.length - 1; i++) {
        patients[i] = data.data[i][1];
        mutations[i] = data.data[i][2];

      }


      var index = 0;

      for (var j = 1; j < patients.length; j++) {



      if(j > 1) {
        if(patients[j-1] == patients[j]) {
          genes[index].push(mutations[j]);
        }
        else {
          index++;
          var array = [];
          people[index] = patients[j];
          array.push(mutations[j]);
          genes[index] = array;
        }
      }
        else {
          var array = [];
          people[index] = patients[j];
          array.push(mutations[j]);
          genes[index] = array;

        }
      }
      var k = 0;
      var rank = [];

      console.log(genes);

      for (var z = 0; z < people.length; z++) {
        var doc = {
               "_id":z.toString(), "patient":people[z], "mutations":genes[z]
                };
      db.put(doc);
      }
      db.get("1").then(function (doc) {
              console.log("all done!");
              console.log(doc);
       });

}
});

}


function mode(array)
{
	console.log(array);
    if(array.length == 0)
    	return null;
    var modeMap = {};
    var maxEl = array[0], maxCount = 1;
    for(var i = 0; i < array.length; i++)
    {
    	var el = array[i];
    	if(modeMap[el] == null)
    		modeMap[el] = 1;
    	else
    		modeMap[el]++;
    	if(modeMap[el] > maxCount)
    	{
    		maxEl = el;
    		maxCount = modeMap[el];
    	}
    }
    console.log(maxEl);
    return maxEl;

}


function similar() {
	Papa.parse("./test.csv", {
        download: true,
        dynamicTyping: true,
        complete: function(data) {

    		for(var i = 1; i < data.data.length - 1; i++) {
  			patients[i] = data.data[i][1];
  			mutations[i] = data.data[i][2];

  		}



  		var genes = {};
  		var people = [];


  		var index = 0;

  		for (var j = 1; j < patients.length; j++) {



  		if(j > 1) {
  			if(patients[j-1] == patients[j]) {
  				genes[index].push(mutations[j]);
  			}
  			else {
  				index++;
  				var array = [];
  				people[index] = patients[j];
  				array.push(mutations[j]);
  				genes[index] = array;
  			}
  		}
  			else {
  				var array = [];
  				people[index] = patients[j];
  				array.push(mutations[j]);
  				genes[index] = array;

  			}
  		}
  		console.log(people);
  		console.log(temp);
   		var k = 0;
  		var rank = [];
  		while (genes[k] !== undefined) {
  		var counter = 0;
  		for (var x = 0; x < genes[k].length; x++) {
  			for (var z = 1; z < mutations.length; z++) {
  				if (genes[k][x] == mutations[z]) {
  					counter++;
  				}

  			}
  		}
      counter = counter - genes[k].length;
  		rank.push(counter);
  		k++;

  	}
    console.log(people);



    // console.log(temp);
  	score(rank, people);

  	}



    });

}




function score(number, patients) {

	for(var i = 0; i < number.length - 1; i++) {
		for(var j = i+1; j < number.length; j++) {
			if (number[i] < number[j]) {
				var temp = number[j];
				number[j] = number[i];
				number[i] = temp;

				var temp2 = patients[j];
				patients[j] = patients[i];
				patients[i] = temp2;
			}
		}
	}

 $('.patinfo').children('tbody').empty();
// var $newDescrip = '<tr><td>'+"No.".bold()+'</td><td>'+"Patients".bold()+'</td><td>'+"Mutations".bold()+'</td><td>';
//   $('.Patients').append($newDescrip);

$( ".progress" ).remove();

 for(j=0; j < 10; j++)
    {
      console.log("i'm here");
         var $formrow = '<tr><td>'+(j+1)+'</td><td>'+patients[j]+'</td><td>'+number[j]+'</td><td>';
        $('.patinfo').children('tbody').append($formrow);
    }


// var string = "Similarity rankings:" + "\n"
//   for(var j = 0; j < patients.length; j++) {
//     string += patients[j] + " had " + number[j] + " matches" + "\n";
//   }

//   console.log(string);

//   // document.querySelector('.results').innerHTML = string;
// document.write(string);



}




function find() {

	if(data.count = 0) {
		loadData();
		console.log(mutations);
	}


	var key = document.getElementById("searchForm").elements["searchItem"].value;
	// Papa.parse("./all_maf_8_col.csv", {
	// download: true,
	// complete: function(data) {
	// 	console.log(data);


if (key) {
var value = [];
	var number = 0;
	console.log(key);
	for (var i = 0; i < mutations.length; i ++) {

		if(mutations[i] == key) {
			value[number] = i;
			number++;
		}
	}
	console.log(value);
	var string = "Patients found: "
	for(var j = 0; j < value.length; j++) {
		console.log(patients[value[j]]);
		string += patients[value[j]] + "," ;
	}
	document.querySelector('.results').innerHTML = string;
}
}
